package com.andreprolo.linktree.ui.links.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.andreprolo.linktree.databinding.LinkItemBinding
import com.andreprolo.linktree.domain.dto.links.LinkDto

class LinkAdapter(private val onItemSelected: (LinkDto) -> Unit)
    : RecyclerView.Adapter<LinkAdapter.LinkViewHolder>() {

    var links: List<LinkDto> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LinkViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = LinkItemBinding.inflate(layoutInflater, parent, false)
        return LinkViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: LinkViewHolder, position: Int) {
        holder.bind(links[position], onItemSelected)
    }

    override fun getItemCount() = links.size

    class LinkViewHolder(private val itemBinding: LinkItemBinding)
        : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(link: LinkDto, onItemSelected: (LinkDto) -> Unit) {
            itemBinding.tvLinkTitle.text = link.title
            itemBinding.tvLinkDescription.text = link.description
            itemBinding.root.setOnClickListener {
                onItemSelected(link)
            }
        }
    }
}