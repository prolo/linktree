package com.andreprolo.linktree.ui.links.list.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.andreprolo.linktree.domain.dto.links.LinkDto
import com.andreprolo.linktree.domain.dto.links.NewLinkDto
import com.andreprolo.linktree.domain.repository.LinkRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ListLinksViewModel : ViewModel() {

    private val _links = MutableLiveData<List<LinkDto>>()
    val links: LiveData<List<LinkDto>>
        get() = _links

    private val _saveResult = MutableLiveData<Unit>()
    val saveResult: LiveData<Unit>
        get() = _saveResult

    private val repository = LinkRepository()

    fun findAll() {
        viewModelScope.launch(Dispatchers.IO) {
            val list = repository.findAll()
            _links.postValue(list)
        }
    }

    fun save(link: NewLinkDto) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.save(link)
            _saveResult.postValue(Unit)
        }
    }

    fun filterByTitle(title: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val list = repository.filterByTitle(title)
            _links.postValue(list)
        }
    }
}