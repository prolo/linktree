package com.andreprolo.linktree.ui.links.list

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.andreprolo.linktree.R
import com.andreprolo.linktree.databinding.ActivityLinksListBinding
import com.andreprolo.linktree.domain.dto.links.LinkDto
import com.andreprolo.linktree.domain.dto.links.NewLinkDto
import com.andreprolo.linktree.ui.links.list.adapter.LinkAdapter
import com.andreprolo.linktree.ui.links.list.viewmodel.ListLinksViewModel
import com.andreprolo.linktree.ui.links.register.NewLinkDialog

class ListLinksActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityLinksListBinding.inflate(layoutInflater)
    }

    private val viewModel by viewModels<ListLinksViewModel>()

    private lateinit var adapter: LinkAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupView()
        setupObservers()

        viewModel.findAll()
    }

    private fun setupView() {
        adapter = LinkAdapter(::onLinkSelect)

        binding.rvLinks.adapter = adapter
        binding.rvLinks.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.rvLinks.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        binding.etSearch.addTextChangedListener { text ->
            viewModel.filterByTitle(text.toString())
        }
    }

    private fun onLinkSelect(link: LinkDto) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link.link))
        startActivity(browserIntent)
    }

    private fun setupObservers() {
        binding.root.setOnClickListener {
            binding.etSearch.clearFocus()
        }

        viewModel.links.observe(this) {
            adapter.links = it
        }
        viewModel.saveResult.observe(this) {
            viewModel.findAll()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.mnNew) {
            NewLinkDialog.createAndShow(supportFragmentManager, ::onSave)
            return true
        }

        return false
    }

    private fun onSave(link: NewLinkDto) {
        viewModel.save(link)
    }
}