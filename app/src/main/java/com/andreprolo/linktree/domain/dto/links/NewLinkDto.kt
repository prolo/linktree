package com.andreprolo.linktree.domain.dto.links

data class NewLinkDto(
    val title: String,
    val description: String,
    val link: String
)
