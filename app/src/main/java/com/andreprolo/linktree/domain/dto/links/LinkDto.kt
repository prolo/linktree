package com.andreprolo.linktree.domain.dto.links

data class LinkDto(
    val id: Int,
    val title: String,
    val description: String,
    val link: String
)
