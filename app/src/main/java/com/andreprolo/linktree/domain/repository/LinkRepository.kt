package com.andreprolo.linktree.domain.repository

import com.andreprolo.linktree.data.dao.LinkDao
import com.andreprolo.linktree.data.db.LinkTreeDb
import com.andreprolo.linktree.data.entity.Link
import com.andreprolo.linktree.domain.dto.links.LinkDto
import com.andreprolo.linktree.domain.dto.links.NewLinkDto

class LinkRepository {

    private val dao: LinkDao by lazy {
        LinkTreeDb.instance.linkDao()
    }

    suspend fun save(newLink: NewLinkDto) {
        val link = Link(
            newLink.title,
            newLink.description,
            newLink.link
        )
        dao.save(link)
    }

    suspend fun findAll(): List<LinkDto> {
        val result = dao.findAll()
        return result.map { link ->
            LinkDto(
                link.id!!,
                link.title,
                link.description,
                link.link
            )
        }
    }

    suspend fun filterByTitle(title: String): List<LinkDto> {
        val result = dao.findByTitle("%${title}%")
        return result.map { link ->
            LinkDto(
                link.id!!,
                link.title,
                link.description,
                link.link
            )
        }
    }
}