package com.andreprolo.linktree.app

import android.app.Application
import com.andreprolo.linktree.data.db.LinkTreeDb

class LinkTreeApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        LinkTreeDb.initialize(this)
    }
}