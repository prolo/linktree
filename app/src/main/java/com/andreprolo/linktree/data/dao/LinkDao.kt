package com.andreprolo.linktree.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.andreprolo.linktree.data.entity.Link

@Dao
interface LinkDao {

    @Insert
    suspend fun save(link: Link)

    @Update
    suspend fun update(link: Link)

    @Query("select * from link")
    suspend fun findAll(): List<Link>

    @Query("select * from link where id = :id")
    suspend fun findById(id: Int) : Link

    @Query("select * from link where title like :title")
    suspend fun findByTitle(title: String): List<Link>
}