package com.andreprolo.linktree.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.andreprolo.linktree.data.dao.LinkDao
import com.andreprolo.linktree.data.entity.Link

@Database(
    entities = [Link::class],
    version = 1
)
abstract class LinkTreeDb : RoomDatabase() {

    abstract fun linkDao() : LinkDao

    companion object {
        private lateinit var mInstance: LinkTreeDb
        val instance: LinkTreeDb
            get() = mInstance

        fun initialize(context: Context) {
            mInstance = Room.databaseBuilder(context,
                LinkTreeDb::class.java, "db_linktree").build()
        }
    }
}